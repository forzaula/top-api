import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { CreateReviewDto } from "../src/review/dto/create-review.dto";
import { Types,disconnect } from "mongoose";
import { REVIEW_NOT_FOUND } from "../src/review/review.constants";

const productId=new Types.ObjectId().toHexString()
const testDto:CreateReviewDto={
  name:"Тест",
  title:"загаловок",
  description:"описание тестовое",
  rating:5,
  productId
}

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let createdId:string

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/review/create (POST) - success', async (done) => {
    return request(app.getHttpServer())
      .post('/review/create')
      .send(testDto)
      .expect(201)
      .then(({body}:request.Response)=>{
        createdId = body._id;
        expect(createdId).toBeDefined();
        done();
      })
  });
  it('/review/create (POST) - fail',  () => {
    return request(app.getHttpServer())
      .post('/review/create')
      .send({...testDto,rating:0})
      .expect(400);
  });
  it('/review/findByProduct/:productId (GET)-success', async (done) => {
    return request(app.getHttpServer())
      .get('/review/findByProduct/' +productId)
      .expect(200)
      .then(({body}:request.Response)=>{
        expect(body.length).toBe(1);
        done();

      })
  });
  it('/review/findByProduct/:productId (GET)-fail', async (done) => {
    return request(app.getHttpServer())
      .get('/review/findByProduct/' +new Types.ObjectId().toHexString())
      .expect(200)
      .then(({body}:request.Response)=>{
        expect(body.length).toBe(0);
        done();

      })
  });
  it('/review/:id (DELETE)-success', async (done) => {
    return request(app.getHttpServer())
      .delete('/review/' +createdId)
      .expect(200)
  });
  it('/review/:id (DELETE)-fail', async (done) => {
    return request(app.getHttpServer())
      .delete('/review/' +new Types.ObjectId().toHexString())
      .expect(404,{
        statusCode:404,
        message:REVIEW_NOT_FOUND
      })
  });
  afterAll(()=>{
    disconnect()
  })
});
