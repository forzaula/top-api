import { Module } from '@nestjs/common';
import { TopPageController } from './top-page.controller';
import { ConfigService } from "@nestjs/config";
import { TypegooseModule } from "nestjs-typegoose";

@Module({
  controllers: [TopPageController],
  providers:[ConfigService],
  imports:[TypegooseModule.forFeature([{
    typegooseClass:TopPageModule,
    schemaOptions:{
      collection:'TopPage'
    }
  }])]
})
export class TopPageModule {}
