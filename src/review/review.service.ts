import { Injectable } from "@nestjs/common";
import { ReviewModule } from "./review.module";
import { ModelType,DocumentType } from "@typegoose/typegoose/lib/types";
import { CreateReviewDto } from "./dto/create-review.dto";
import { InjectModel } from "nestjs-typegoose";
import { ReviewModel } from "./review.model";
import { Types } from "mongoose";



@Injectable()
export class ReviewService {
  constructor(@InjectModel(ReviewModel) private readonly reviewModel:ModelType<ReviewModel>) {}

  async create(dto:CreateReviewDto):Promise<DocumentType<ReviewModule>>{
    return this.reviewModel.create(dto)
  };
  async delete(id:string):Promise<DocumentType<ReviewModule> | null>{
    return this.reviewModel.findByIdAndDelete(id).exec();
  };
  async findByProductId(productId:string):Promise<DocumentType<ReviewModule>[]>{
    return this.reviewModel.find({productId:productId }).exec();
};
  async deleteByProductId(productId:string){
    return this.reviewModel.deleteMany({productId:productId}).exec()
  }
}
